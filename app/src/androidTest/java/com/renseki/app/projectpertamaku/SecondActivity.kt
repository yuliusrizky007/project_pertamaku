package com.renseki.app.projectpertamaku

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    companion object {
        fun getStartIntent(
                context: Context,
                us: String
        ) = Intent(context, SecondActivity::class.java).apply {
            putExtra("user", us)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val user_login = intent.getStringExtra("user")
        hasil.text = "Welcome, $user_login"
    }
}
