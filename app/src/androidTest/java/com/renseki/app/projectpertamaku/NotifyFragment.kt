package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_notify.*

class NotifyFragment : Fragment() {

    companion object {
        fun newInstance() = NotifyFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
                R.layout.fragment_notify,
                container,
                false
        )
    }

    fun cekLogin(tulisan: String) {
        text_status.append("$tulisan\n")
    }
}
