package com.renseki.app.projectpertamaku

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.renseki.app.projectpertamaku.NotifyFragment

class MainActivity : AppCompatActivity(), InputLoginFragment.InputActionListener {

    private var password_seharusnya = "stts"
    private lateinit var NotifyFrag: NotifyFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        untukFragAtas()
        untukFragBawah()
    }

    private fun untukFragAtas() {
        NotifyFrag = NotifyFragment.newInstance()
        supportFragmentManager.beginTransaction().apply {
            replace(
                    R.id.frame2,
                    NotifyFrag
            )
            commit()
        }
    }

    private fun untukFragBawah() {
        val fragment = InputLoginFragment.newInstance(this)
        val begin1 = supportFragmentManager.beginTransaction()
        begin1.replace(
                R.id.frame1, fragment
        )
        begin1.commit()
    }

    override fun getLogin(us: String, pw: String) {
        Toast.makeText(
                this,
                us,
                Toast.LENGTH_SHORT
        ).show()
        if (us.isEmpty() || pw.isEmpty()) {
            NotifyFrag.cekLogin("Username dan Password tidak boleh kosong!")
        } else if (pw.equals(password_seharusnya)) {
            openAnotherActivity(us)
        } else {
            NotifyFrag.cekLogin("Password harus 'stts'")
        }
    }

    private fun openAnotherActivity (us: String) {
        val intent = SecondActivity.getStartIntent(this, us)
        startActivity(intent)
    }

}
